/**
 * Unit interface is the unit of something.
 * @author Voraton Lertrattanapaisal
 *
 */
public interface Unit {
	/**
	 * To convert amount to other unit.
	 * @param amt is amount to convert.
	 * @param unit is other unit that you want to convert to.
	 */
	public double convertTo(double amt,Unit unit);
	/**
	 * To get base value of unit.
	 */
	public double getValue();
	/**
	 * To return unit name.
	 */
	public String toString();
}
