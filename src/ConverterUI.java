import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * Distance Converter Program.
 * @author Voraton Lertrattanapaisal
 *
 */
public class ConverterUI extends JFrame{
	private UnitConverter uc;
	private JComboBox fromBox;
	private JComboBox toBox;
	private JTextField fromAmount;
	private JTextField toAmount;
	private JButton conBut;
	private JButton clearBut;
	/**
	 * Constructor for create new converter.
	 * @param uc is unit converter for convert thing in background.
	 */
	public ConverterUI( UnitConverter uc){
		super("Distance Converter");
		this.uc=uc;
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		initComponent();
	}
	/**
	 * To initialize the component.
	 */
	private void initComponent(){
		Container content = getContentPane();
		content.setLayout(new FlowLayout());
		fromAmount = new JTextField(10);
		fromBox = new JComboBox(uc.getUnits());
		toAmount = new JTextField(10);
		toAmount.setEditable(false);
		toBox = new JComboBox(uc.getUnits());
		conBut = new JButton("Convert");
		clearBut = new JButton("Clear");
		conBut.addActionListener(new ConvertButtonListener());
		clearBut.addActionListener(new ClearButtonListener());
		fromAmount.addActionListener(new ConvertButtonListener());
		content.add(fromAmount);
		content.add(fromBox);
		content.add(toAmount);
		content.add(toBox);
		content.add(conBut);
		content.add(clearBut);
		pack();	
	}
	/**
	 * For run the windows of converter.
	 */
	public void run(){
		setVisible(true);
	}
	/**
	 * ActionListener of Convert Button for convert data.
	 * @author Voaton Lertrattanapaisal
	 *
	 */
	private class ConvertButtonListener implements ActionListener {
		public void actionPerformed (ActionEvent e){
			try{
				double from = Double.parseDouble(fromAmount.getText().trim());
				if (from <0){
					throw new IllegalArgumentException();
				}
				else {
					toAmount.setText(String.format("%.4g",uc.convert(from, (Unit)fromBox.getSelectedItem(), (Unit)toBox.getSelectedItem())));
				}
			}
			catch(NumberFormatException er){
				JOptionPane.showMessageDialog( null,fromAmount.getText()+" is incorrect data!","Error!",JOptionPane.INFORMATION_MESSAGE);
			}
			catch (IllegalArgumentException c){
				JOptionPane.showMessageDialog( null,"Data can't be negative!","Error!",JOptionPane.INFORMATION_MESSAGE);
			}
			catch(Exception o){
				JOptionPane.showMessageDialog( null,"Error","Error!",JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
	/**
	 * ActionListener for Clear Button for clear data.
	 * @author Voraton Lertrattanapaisal
	 *
	 */
	private class ClearButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			fromAmount.setText("");
			toAmount.setText("");
		}
	}
}
